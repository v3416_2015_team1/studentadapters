﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class Student : User
    {
        public int? GroupId { set; get; }
        [Display(Name = "Название группы")]
        public virtual Group Group { set; get; }
    }
}
