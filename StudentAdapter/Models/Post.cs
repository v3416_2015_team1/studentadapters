﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;


namespace StudentAdapter.Models
{
    public class Post
    {
        public int Id { set; get; }
        public int? AdapterId { set; get; }
        public Adapter Adapter { set; get; }
        public string Header { set; get; }
        public string Description { set; get; }
        public virtual ICollection<LikeList> LikeLists { set; get; }
        public string DocPath { set; get; }
    }
}