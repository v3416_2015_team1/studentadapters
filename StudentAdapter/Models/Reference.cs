﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;

namespace StudentAdapter.Models
{
    public class Reference
    {
        [AllowHtml]
        public string Text { set; get; }
    }
}