﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StudentAdapter.Models
{
    public class LikeList
    {
        public int Id { set; get; }
        public int? UserId { set; get; }
        public virtual User User { set; get; }
        public int? PostId { set; get; }
        public virtual Post Post { set; get; }
    }
}