﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class GroupsList
    {
        public int Id { set; get; }
        public int AdapterId { set; get; }
        public int GroupId { set; get; }
    }
}
