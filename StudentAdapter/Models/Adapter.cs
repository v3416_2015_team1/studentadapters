﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class Adapter: User
    {
        public string AvatarPath { set; get; }
        public int Likes { set; get; }
        public int Points { set; get; }
        public int Money { set; get; }
        public int? GroupId { set; get; }
        public Group Group { set; get; }
        public virtual ICollection<AwardList> AwardLists { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
        public int? Part { set; get; }
    }
}
