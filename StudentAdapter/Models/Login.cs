﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentAdapter.Models
{
    public class Login
    {
        [Required]
        [Display(Name = "Логин")]
        public string UserName { set; get; }
        
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { set; get; }
    }
}