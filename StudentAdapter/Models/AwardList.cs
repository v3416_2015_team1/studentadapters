﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentAdapter.Models
{
    public class AwardList
    {
        public int Id { set; get; }
        public int? AdapterId { set; get; }
        public virtual Adapter Adapter { set; get; }
        public int? AwardId { set; get; }
        public virtual Award Award { set; get; }
    }
}