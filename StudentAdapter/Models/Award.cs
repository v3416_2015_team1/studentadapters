﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class Award
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string ImagePath { set; get; }
    }
}
