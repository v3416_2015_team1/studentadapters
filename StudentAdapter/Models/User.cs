﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace StudentAdapter.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class User
    {
        public int Id { set; get; }
        [Display(Name = "Логин")]
        public string UserName { set; get; }
        
        [Display(Name = "Пароль")]
        public string Password { set; get; }
        public int? RoleId { get; set; }
        public virtual Role Role { set; get; }
        [Display(Name = "Имя")]
        public string Name { set; get; }
        [Display(Name = "Фамилия")]
        public string Surname { set; get; }
    }
}
