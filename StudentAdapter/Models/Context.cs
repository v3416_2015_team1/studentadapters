﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace StudentAdapter.Models
{
    public class ContextInitializer : CreateDatabaseIfNotExists<Context>
    {
        protected override void Seed(Context db)
        {
            db.Roles.Add(new Role { Id = 1, Name = "Admin" });
            db.Roles.Add(new Role { Id = 2, Name = "Adapter" });
            db.Roles.Add(new Role { Id = 3, Name = "Student" });

            db.Awards.Add(new Award
            {
                Id = 1,
                Name = "Бронзовая медаль",
                ImagePath = "~/Files/Service/Icons/BronzeMedal.jpg"
            });
            db.Awards.Add(new Award
            {
                Id = 2,
                Name = "Серебряная медаль",
                ImagePath = "~/Files/Service/Icons/SilverMedal.jpg"
            });
            db.Awards.Add(new Award
            {
                Id = 3,
                Name = "Золотая медаль",
                ImagePath = "~/Files/Service/Icons/GoldMedal.jpg"
            });
            db.Awards.Add(new Award
            {
                Id = 4,
                Name = "Звезда",
                ImagePath = "~/Files/Service/Icons/Star.jpg"
            });
            db.Awards.Add(new Award
            {
                Id = 5,
                Name = "Премия Оскар",
                ImagePath = "~/Files/Service/Icons/AcademyAward.jpg"
            });

            db.Admins.Add(new Admin
            {
                AvatarPath = "~/Files/Admins/Kris/Kris.jpg",
                UserName = "Kris",
                Password = "kris",
                Name = "Кристина",
                Surname = "Тишкина",
                Email = "Kris@gmail.ru",
                RoleId = 1
            });

            /*db.Groups.Add(new Group
            {
                Id = 1,
                GroupName = "v3416",
            });

            db.Students.Add(new Student
            {
                UserName = "vano",
                Password = "vano",
                GroupId = 1,
                Name = "Ваня",
                Surname = "Яхнев",
                RoleId = 3,
            });
            db.Adapters.Add(new Adapter
            {
                AvatarPath = "~/Files/Adapters/Sveta/Sveta.jpg",
                UserName = "sveta",
                Password = "sveta",
                GroupId = 1,
                Name = "Света",
                Surname = "Дронникова",
                RoleId = 2,
                Points = 20,
                Money = 15,
                Likes = 10
            });

            db.Goods.Add(new Good
            {
                Name = "Футболка",
                Description = "Крутая футболка с логотипом!",
                Picture = "~/Files/Service/Shop/Tshirt.jpg",
                OnStock = true,
                Price = 10
            });
            db.Goods.Add(new Good
            {
                Name = "Portal-пушка",
                Description = "Такую должен иметь каждый адаптер!",
                Picture = "~/Files/Service/Shop/Portalgun.jpg",
                OnStock = false,
                Price = 999
            });*/
            base.Seed(db);
            db.SaveChanges();
        }
    }

    public class Context : DbContext
    {

        static Context()
        {
            Database.SetInitializer<Context>(new ContextInitializer());
        }

        public Context()
            : base("Context")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Adapter> Adapters { get; set; }
        public DbSet<Good> Goods { get; set; }
        public DbSet<OrdersList> OrdersLists { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<AwardList> AwardLists { set; get; }
        public DbSet<Group> Groups { set; get; }
        public DbSet<Post> Posts { set; get; }
        public DbSet<LikeList> LikeLists { set; get; }
    }
}
