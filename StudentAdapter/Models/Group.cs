﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace StudentAdapter.Models
{
    public class Group
    {
        public int Id { set; get; }
        [Display(Name = "Название группы")]
        public string GroupName { set; get; }
        public virtual ICollection<Student> Students { set; get; }
        public virtual ICollection<Adapter> Adapters { set; get; }
    }
}