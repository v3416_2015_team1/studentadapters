﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class Admin : User
    {
        public string AvatarPath { set; get; }
        [Display(Name = "E-mail")]
        public string Email { set; get; }
    }
}
