﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class Good
    {
        public int Id { set; get; }
        [Required]
        [Display(Name = "Изображение")]
        public string Picture { set; get; }
        [Required]
        [Display(Name = "Цена")]
        public int Price { set; get; }
        [Required]
        [Display(Name = "Название")]
        public string Name { set; get; }
        [Display(Name = "Описание")]
        public string Description { set; get; }
        [Required]
        [Display(Name = "Есть ли на складе?")]
        public bool OnStock { set; get; }
    }
}
