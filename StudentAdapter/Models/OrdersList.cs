﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdapter.Models
{
    public class OrdersList
    {
        public int Id { set; get; }
        public DateTime Date { set; get; }
        public int? AdapterId { set; get; }
        public virtual Adapter Adapter { set; get; }
        public int? GoodId { set; get; }
        public virtual Good Good { set; get; }
    }
}
