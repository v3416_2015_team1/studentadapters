<br>Каждый из нас был первокурсником. Нам было страшно и волнительно появляться в стенах университета. Такие странные однокурсники, запутанные коридоры, информационный вакуум... А теперь представь, как было бы здорово, если бы у тебя был помощник!
<br>
<br>И сейчас у тебя есть шанс стать путеводителем, опорой и другом для десятков пока еще школьников, а уже этой осенью - студентов Университета ИТМО.</li> 
<br>
<br>В нашем вузе существует проект "Адаптер Университета ИТМО". 
<br>
<br>Адаптер - это студент, который будет курировать группу первокурсников-2015, проводить для них различные мероприятия, обеспечивать необходимой информацией о жизни в университете и решать возникающие вопросы.
<br>
Подай заявку, заполнив анкету, и, может, именно ты станешь Адаптером Университета ИТМО!