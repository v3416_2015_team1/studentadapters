﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentAdapter.Models;
using System.Data.Entity;

namespace StudentAdapter.Controllers
{
    [Authorize(Roles = "Adapter")]
    public class AdapterController : SharedController
    {        
        // работа со страницей

        public ActionResult MyPage()
        {
            return View(db.Adapters.Find(User.Identity.GetUserId<int>()));
        }

        [HttpGet]
        public ActionResult EditMyPage()
        {
            return View(db.Adapters.Find(User.Identity.GetUserId<int>()));
        }

        [HttpPost]
        public ActionResult EditMyPage(Adapter adapter)
        {
            db.Entry(adapter).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("MyPage");
        }

        public ActionResult LoadImage(HttpPostedFileBase image, int? adapterId)
        {
            if (image != null)
            {
                var adapter = db.Adapters.Find(adapterId);
                if (adapter.AvatarPath.CompareTo("~/Files/Service/Icons/Default.jpg") != 0)
                {
                    System.IO.File.Delete(Server.MapPath(adapter.AvatarPath));
                }
                string path = "~/Files/Adapters/" + User.Identity.Name + "/" + image.FileName;
                image.SaveAs(Server.MapPath(path));
                adapter.AvatarPath = path;
                db.Entry(adapter).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("MyPage");
        }

        // работа с записями

        public ActionResult CreatePost(string header, string description, int? adapterId, HttpPostedFileBase doc)
        {
            string DocPath = "";
            if (doc != null)
            {
                DocPath = "~/Files/Adapters/" + User.Identity.Name + "/" + doc.FileName;
                doc.SaveAs(Server.MapPath(DocPath));
            }
            db.Posts.Add(new Post {
                AdapterId = User.Identity.GetUserId<int>(),
                Header = header,
                Description = description,
                DocPath = DocPath
            });
            db.SaveChanges();
            return RedirectToAction("MyPage");
        }

        // работа с магазином

        public ActionResult BuyGood(int? goodId)
        {
            var adapter = db.Adapters.Find(User.Identity.GetUserId<int>());
            var price = db.Goods.Find(goodId).Price;
            adapter.Money -= price;
            db.Entry(adapter).State = EntityState.Modified;
            db.OrdersLists.Add(new OrdersList
            {
                Date = DateTime.Now,
                GoodId = goodId,
                AdapterId = User.Identity.GetUserId<int>()
            });

            db.SaveChanges();
            return RedirectToAction("Store");
        }

        public ActionResult Cancel(int? ordersListId)
        {
            var order = db.OrdersLists.Find(ordersListId);
            order.Adapter.Money += order.Good.Price;
            db.Entry(order.Adapter).State = EntityState.Modified;
            db.OrdersLists.Remove(order);
            db.SaveChanges();
            return RedirectToAction("OrdersList", order.AdapterId);
        }

        public ActionResult OrdersList()
        {
            int adapterId = User.Identity.GetUserId<int>();
            return View(db.OrdersLists.Where(orders => orders.AdapterId == adapterId).ToList());
        }
    }
}
