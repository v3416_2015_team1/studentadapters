﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentAdapter.Models;
using System.Data.Entity;

namespace StudentAdapter.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentController : SharedController
    {
        public ActionResult MyPage()
        {
            return View(db.Students.Find(User.Identity.GetUserId<int>()));
        }

        [HttpGet]
        public ActionResult EditMyPage()
        {
            return View(db.Students.Find(User.Identity.GetUserId<int>()));
        }

        [HttpPost]
        public ActionResult EditMyPage(Student student)
        {
            db.Entry(student).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("MyPage");
        }
    }
}
