﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentAdapter.Models;
using System.Collections;
using System.Data.Entity;

namespace StudentAdapter.Controllers
{
    public class SharedController : Controller
    {
        public Context db = new Context();

        public string getViewNameFromCookie(string action)
        {
            string rights = (User.Identity.GetUserRole() == "") ? "Default" : User.Identity.GetUserRole();
            return "~/Views/" + rights + "/" + action + ".cshtml";
        }

        [HttpGet]
        public ActionResult AdaptersList()
        {
            var adapterList = db.Adapters.ToList();
            adapterList.Sort((x, y) => y.Points.CompareTo(x.Points));
            if (adapterList.Count != 0)
            {
                int max = adapterList.First().Points;
                for (int i = 0; i < adapterList.Count(); ++i)
                {
                    adapterList[i].Part = (int)Math.Truncate((double)adapterList[i].Points * 100 / (double)max);
                }
            }
            ViewBag.AdaptersList = adapterList;
            return View(getViewNameFromCookie("AdaptersList"));
        }

        public ActionResult AdapterPage(int? adapterId)
        {
            ViewBag.Awards = db.Awards.ToList();
            if (User.Identity.GetUserRole() == "Adapter" && User.Identity.GetUserId<int>() == adapterId)
            {
                return View(getViewNameFromCookie("MyPage"), db.Adapters.Find(adapterId));
            }
            return View(getViewNameFromCookie("AdapterPage"), db.Adapters.Find(adapterId));
        }

        [HttpGet]
        public ActionResult Contacts()
        { 
            return View(getViewNameFromCookie("Contacts"), db.Admins);
        }

        [HttpGet]
        public ActionResult Error()
        {
            return View(getViewNameFromCookie("Error"));
        }

        [HttpGet]
        public ActionResult Reference()
        {
            System.IO.StreamReader file = new System.IO.StreamReader(Server.MapPath("~/Files/Service/Reference.txt"));
            Reference Reference = new Reference();
            Reference.Text = file.ReadToEnd();
            file.Close();
            return View(getViewNameFromCookie("Reference"), Reference);
        }

        [HttpGet]
        public ActionResult Store()
        {
            if (User.Identity.GetUserRole() == "Adapter")
            {
                ViewBag.Money = db.Adapters.Find(User.Identity.GetUserId<int>()).Money;
            }
            return View(getViewNameFromCookie("Store"), db.Goods.ToList());
        }

        // действия со страницы адаптера

        [Authorize(Roles = "Adapter, Admin, Student")]
        public FileResult GetDoc(string filePath, string fileName)
        {
            return File(Server.MapPath(filePath), "application/pdf", fileName + ".pdf");
        }

        [Authorize(Roles = "Adapter, Student")]
        public ActionResult Like(int? adapterId, int? postId)
        {
            db.Adapters.Find(adapterId).Likes++;
            db.Adapters.Find(adapterId).Points++;
            db.LikeLists.Add(new LikeList
            {
                PostId = postId,
                UserId = User.Identity.GetUserId<int>()
            });
            db.SaveChanges();
            return RedirectToAction("AdapterPage", new { adapterId = adapterId });
        }
    }
}
