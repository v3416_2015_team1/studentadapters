﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentAdapter.Models;
using System.Data.Entity;

namespace StudentAdapter.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : SharedController
    {
        // работа с группами

        private bool checkUserName(string userName)
        {
            return db.Users.Any(u => (u.UserName.Equals(userName)));
        }

        public ActionResult UserManager()
        {
            var groups = db.Groups.ToList();
            return View(groups);
        }

        [HttpGet]
        public ActionResult CreateGroup()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateGroup(Group group)
        {
            db.Groups.Add(group);
            db.SaveChanges();
            return RedirectToAction("UserManager");
        }

        [HttpGet]
        public ActionResult CreateAdmin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAdmin(Admin admin)
        {
            if (checkUserName(admin.UserName))
            {
                ModelState.AddModelError("", "Логин используется");
                return View();
            }
            admin.RoleId = 1;
            admin.AvatarPath = "~/Files/Service/Icons/Default.jpg";
            db.Admins.Add(admin);
            db.SaveChanges();
            System.IO.Directory.CreateDirectory(Server.MapPath("~/Files/Admins/" + admin.UserName + "/"));
            return RedirectToAction("UserManager");
        }

        [HttpGet]
        public ActionResult EditGroup(int? groupId)
        {
            return View(db.Groups.Find(groupId));
        }

        [HttpPost]
        public ActionResult EditGroup(Group group)
        {
            db.Entry(group).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("UserManager");
        }

        private void removeStudent(User user)
        {
            var userId = user.Id;
            db.LikeLists.RemoveRange(db.LikeLists.Where(likeList => likeList.UserId == userId));
            db.Users.Remove(user);
        }


        private void removeAdapter(User user)
        {
            var userId = user.Id;
            db.AwardLists.RemoveRange(db.AwardLists.Where(awardList => awardList.AdapterId == userId));
            db.OrdersLists.RemoveRange(db.OrdersLists.Where(ordersList => ordersList.AdapterId == userId));
            db.LikeLists.RemoveRange(db.LikeLists.Where(likeList => likeList.Post.AdapterId == userId));
            db.Posts.RemoveRange(db.Posts.Where(post => post.AdapterId == userId));
            System.IO.Directory.Delete(Server.MapPath("~/Files/Adapters/" + user.UserName + "/"), true);
            removeStudent(user);
        }

        public ActionResult RemoveGroup(int? groupId)
        {
            var group = db.Groups.Find(groupId);
            db.Groups.Remove(group);

            foreach (var user in db.Students.Where(student => student.GroupId == groupId).ToList())
            {
                removeStudent(user);
            }

            foreach (var user in db.Adapters.Where(adapter => adapter.GroupId == groupId).ToList())
            {
                removeAdapter(user);
            }

            db.SaveChanges();
            return RedirectToAction("UserManager");
        }

        public ActionResult RemoveUser(int? userId)
        {
            var user = db.Users.Find(userId);
            if (user.RoleId == 2)
                removeAdapter(user);
            else
                removeStudent(user);
            db.SaveChanges();
            return RedirectToAction("UserManager");
        }

        [HttpGet]
        public ActionResult CreateAdapter(int? groupId)
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAdapter(Adapter adapter)
        {
            if (checkUserName(adapter.UserName))
            {
                ModelState.AddModelError("", "Логин используется");
                return View();
            }
            adapter.Points = adapter.Likes = adapter.Money = 0;
            adapter.GroupId = Int32.Parse(Request["groupId"]);
            adapter.AvatarPath = "~/Files/Service/Icons/Default.jpg";
            adapter.RoleId = 2;
            db.Adapters.Add(adapter);
            db.SaveChanges();
            System.IO.Directory.CreateDirectory(Server.MapPath("~/Files/Adapters/" + adapter.UserName + "/"));
            return RedirectToAction("UserManager");
        }

        [HttpGet]
        public ActionResult CreateStudent(int? groupId)
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateStudent(Student student)
        {
            if (checkUserName(student.UserName))
            {
                ModelState.AddModelError("", "Логин используется");
                return View();
            }
            student.GroupId = Int32.Parse(Request["groupId"]);
            student.RoleId = 3;
            db.Students.Add(student);
            db.SaveChanges();
            return RedirectToAction("UserManager");
        }

        // работа со страницей адаптера

        public ActionResult GivePoints(int? adapterId, int points)
        {
            if (db.Adapters.Find(adapterId).Money + points < 0)
            {
                db.Adapters.Find(adapterId).Points = 0;
            }
            else
            {
                if (points > 0)
                {
                    db.Adapters.Find(adapterId).Money += points;
                }
                db.Adapters.Find(adapterId).Points += points;
            }
            db.SaveChanges();
            return RedirectToAction("AdapterPage", new { adapterId = adapterId });
        }

        public ActionResult GiveAward(int? adapterId, int? awardId)
        {
            db.AwardLists.Add(new AwardList
            {
                AdapterId = adapterId,
                AwardId = awardId
            });
            db.SaveChanges();
            return RedirectToAction("AdapterPage", new { adapterId = adapterId });
        }

        // работа со страницей администратора

        public ActionResult MyPage()
        {
            return View(db.Admins.Find(User.Identity.GetUserId<int>()));
        }

        public ActionResult LoadImage(HttpPostedFileBase image, int? adminId)
        {
            if (image != null)
            {
                var admin = db.Admins.Find(adminId);
                if (admin.AvatarPath.CompareTo("~/Files/Service/Icons/Default.jpg") != 0)
                {
                    System.IO.File.Delete(Server.MapPath(admin.AvatarPath));
                }
                string path = "~/Files/Admins/" + User.Identity.Name + "/" + image.FileName;
                image.SaveAs(Server.MapPath(path));
                admin.AvatarPath = path;
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("MyPage");
        }

        [HttpGet]
        public ActionResult EditMyPage(bool isLastAdmin = false)
        {
            if (isLastAdmin)
            {
                ModelState.AddModelError("", "Вы последний администратор");
            }
            return View(db.Users.Find(User.Identity.GetUserId<int>()));
        }

        [HttpPost]
        public ActionResult EditMyPage(Admin admin)
        {

            db.Entry(admin).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("MyPage");
        }

        public ActionResult RemoveAdmin()
        {
            if (db.Admins.Count() == 1)
            {
                return RedirectToAction("EditMyPage", new {isLastAdmin = true});
            }
            var user = db.Users.Find(User.Identity.GetUserId<int>());
            System.IO.Directory.Delete(Server.MapPath("~/Files/Admins/" + user.UserName + "/"), true);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Login", "Default");
        }
        
        // работа с заказами

        public ActionResult OrdersList()
        {
            var orders = db.OrdersLists.ToList();
            return View(orders);
        }

        public ActionResult GoodGiven(int? ordersListId)
        {
            var order = db.OrdersLists.Find(ordersListId);
            db.OrdersLists.Remove(order);
            db.SaveChanges();
            return RedirectToAction("OrdersList");
        }

        public ActionResult Cancel(int? ordersListId)
        {
            var order = db.OrdersLists.Find(ordersListId);
            order.Adapter.Money += order.Good.Price;
            db.Entry(order.Adapter).State = EntityState.Modified;
            db.OrdersLists.Remove(order);
            db.SaveChanges();
            return RedirectToAction("OrdersList");
        }

        // работа со справкой

        [HttpGet]
        public ActionResult EditReference()
        {
            System.IO.StreamReader file = new System.IO.StreamReader(Server.MapPath("~/Files/Service/Reference.txt"));
            Reference Reference = new Reference();
            Reference.Text = file.ReadToEnd();
            file.Close();
            return View(Reference);
        }

        [HttpPost]
        public ActionResult EditReference(Reference reference)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/Files/Service/Reference.txt"));
            file.Write(reference.Text);
            file.Close();
            return RedirectToAction("Reference");
        }

        // работа с магазином

        [HttpGet]
        public ActionResult CreateGood()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateGood(Good model, HttpPostedFileBase file, string onStock)
        {
            string path;
            if (file == null)
                path = "~/Files/Service/Icons/Default.jpg";
            else
            {
                path = "~/Files/Service/Shop/" + file.FileName;
                file.SaveAs(Server.MapPath(path));
            }
            model.Picture = path;
            model.OnStock = Boolean.Parse(onStock);
            if (model.Price < 0)
            {
                ModelState.AddModelError("", "Цена не может быть отрицательной");
                return View();
            }
            db.Goods.Add(model);
            db.SaveChanges();
            return RedirectToAction("Store");
        }

        public ActionResult RemoveGood(int? goodId)
        {
            var good = db.Goods.Find(goodId);
            db.OrdersLists.RemoveRange(db.OrdersLists.Where(orderList => orderList.GoodId == goodId));
            db.Goods.Remove(good);
            db.SaveChanges();

            return RedirectToAction("Store");
        }

        public ActionResult InStock(int? goodId)
        {
            db.Goods.Find(goodId).OnStock = true;
            db.SaveChanges();
            return RedirectToAction("Store");
        }

        public ActionResult NotInStock(int? goodId)
        {
            db.Goods.Find(goodId).OnStock = false;
            db.SaveChanges();
            return RedirectToAction("Store");
        }
    }
}
