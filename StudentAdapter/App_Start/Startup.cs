﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using StudentAdapter.Models;

[assembly: OwinStartup(typeof(StudentAdapter.Startup))]

namespace StudentAdapter
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Default/Login")
            });
        }
    }
}
