# Student Adapters #
Using: ?

Спецификация: https://docs.google.com/document/d/1METMNbMJl8u5l9FVaGkG2xwtFSUHhyTk-qucNGxeWus/edit?usp=sharing

План проекта:
https://docs.google.com/document/d/1A6qhDGZFl3gnZ6ul3Xsxil6yicTtE3KKi0LnlR1ROTU/edit?usp=sharing

Методика тестирования:
https://docs.google.com/document/d/1H-t9HU4LCaniJQ3VwFfaVo_mAMaYGyqpwGPQykdjPac/edit?usp=sharing
## Experts: ##
* Тишкина Кристина
* Яхнев Иван
## Programmers: ##
* Дронникова Светлана
* Лукашевич Кирилл
## Testers: ##
* Недошивина Любовь
* Рудаков Николай

## Get Started ##
* Открываем командную строку
* Прописываем путь к директории, в которой будут храниться проекты
ex: cd C:/Projects
* Клонируем репозиторий так, как написано под ведром на синем фоне
* Далее для push и pull запросов нужно так-же зайти в директорию через командную строку и прописать соответственно git push и git pull
## Get Started 2 ##
* Качаем GitGui 
https://github.com/git-for-windows/git/releases/
* В контекстном меню выбираем Git Gui
* Прописываем пути

Второй вариант попроще.